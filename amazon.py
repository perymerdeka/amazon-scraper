# import module
import requests
import os
import json
import threading
import concurrent.futures
from bs4 import BeautifulSoup
import pandas as pd

# getting session
session = requests.Session()

data_product = []

# define base url
base_url = 'https://www.amazon.com'

# development
# keywords = 'camera'

# define index to count detail item
index = 0

# header section
headers = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
    'cache-control': 'max-age=0',
    'cookie': 'session-id=141-8867584-9461869; session-id-time=2082787201l; i18n-prefs=USD; skin=noskin; SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1; ubid-main=133-5952635-4271309; csm-hit=tb:BHHNQ4Q4NAADQB24CTCV+s-R3M3Z82CB248F1N6H9GS|1601256208130&t:1601256208130&adb:adblk_yes; session-token=V01j8RdGTv9Hl6JUOWJi/j6tqHzvRnnUzhlxNs5un/Oo8aDwMZwBswVwBjO9Z5wuzZ/5XLTVh5nTOYOuuKoGIbex6lhP03OBTuiY3n0/1JAIMIcheLaec2QYot+V1nkTidAzJw5OWT4htyKlOlW4SKbuHSMdOYuftxDoKTiUMF6laLojfnNMZ6UJWeeu1t2n1/MlkK39GFrzmD+ySq7DZVJsAejDDG9sOyb+YPG6pojHGigDM9CwBRhtT+pcuEuG++Y+RxBkIsM=; sp-cdn="L5Z9:CA"',
    'downlink': '1.35',
    'ect': '3g',
    'referer': 'https://www.amazon.com/',
    'rtt': '350',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36'
}


def get_session_location():
    print('get location session')

    # header section
    headers_session = {
        'accept': 'text/html,*/*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
        'content-length': '138',
        'content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'cookie': 'session-id=141-8867584-9461869; session-id-time=2082787201l; i18n-prefs=USD; skin=noskin; SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1; ubid-main=133-5952635-4271309; session-token=V01j8RdGTv9Hl6JUOWJi/j6tqHzvRnnUzhlxNs5un/Oo8aDwMZwBswVwBjO9Z5wuzZ/5XLTVh5nTOYOuuKoGIbex6lhP03OBTuiY3n0/1JAIMIcheLaec2QYot+V1nkTidAzJw5OWT4htyKlOlW4SKbuHSMdOYuftxDoKTiUMF6laLojfnNMZ6UJWeeu1t2n1/MlkK39GFrzmD+ySq7DZVJsAejDDG9sOyb+YPG6pojHGigDM9CwBRhtT+pcuEuG++Y+RxBkIsM=; sp-cdn="L5Z9:CA"; csm-hit=tb:BHHNQ4Q4NAADQB24CTCV+s-BWWACX153MC90SNE9SK0|1601256502465&t:1601256502465&adb:adblk_yes',
        'downlink': '1.25',
        'ect': '3g',
        'origin': 'https://www.amazon.com',
        'referer': 'https://www.amazon.com/s?k=iphone&refresh=1&ref=glow_cls',
        'rtt': '350',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',
    }

    # form data
    data = {
        'locationType': 'COUNTRY',
        'district': 'AU',
        'countryCode': 'AU',
        'storeContext': 'generic',
        'deviceType': 'web',
        'pageType': 'Search',
        'actionSource': 'glow',
        'almBrandId': 'undefined'
    }

    # getting session location
    url = 'https://www.amazon.com/gp/delivery/ajax/address-change.html'
    res = session.post(url, data=data, headers=headers_session)
    return res


def get_total_pages(keywords):
    get_session_location()
    params = {
        'k': keywords,
        'ref': 'nb_sb_noss'
    }

    # url
    url = 'https://www.amazon.com/s?'
    res = session.get(url, params=params, headers=headers)

    # check response
    # f = open('tes.html', 'w')
    # f.write(res.text)
    # f.close()

    # parser
    soup = BeautifulSoup(res.text, 'html5lib')
    pagination_area = soup.find('ul', attrs={'class': 'a-pagination'})
    pages = pagination_area.findAll('li', attrs={'class': 'a-disabled'})
    pages = pages[1].text.strip()

    # total pages
    return int(pages)


def get_product_items(keywords, page):
    # getting session
    get_session_location()


    # parameters
    params = {
        'k': keywords,
        'page': str(page),
        'qid': '1601261812',
        'ref': 'sr_pg_2'
    }

    # requests
    url = 'https://www.amazon.com/s'
    res = session.get(url, params=params, headers=headers)

    # checking response
    f = open('./resp.html', 'w')
    f.write(res.text)
    f.close()
    print('cheking response complete')
    # print(res.status_code)

    soup = BeautifulSoup(res.text, 'html5lib')
    section_product = soup.findAll('div', attrs={'class':'a-section a-spacing-medium'})
    # getting all data

    # get list to append data product and global detail
    list_product = []
    for product in section_product:
        item_name = product.find('span', attrs={'class':'a-size-medium a-color-base a-text-normal'}).text.strip()
        refer_link = product.find('a', attrs={'class':'a-link-normal a-text-normal'})['href']
        data_link = base_url + refer_link
        image_url = product.find('div', attrs={'class':'a-section aok-relative s-image-fixed-height'}).find('img', attrs={'class':'s-image'})['src']
        # try:
        #     rating = product.find('span', attrs={'class': 'a-icon-alt'}).text.strip()
        #     # raise Exception(rating)
        # except:
        #     rating = 'no rating previewed'


        # save to dictionary
        data_dict = {
            'image url':image_url,
            'item name':item_name,
            'item link':data_link,
            # 'rating':rating,
        }

        list_product.append(data_dict)
        # print(list_product)
        # raise Exception(data_dict)

        # create directory to save product list
    try:
        print('creating directory to save result ....')
        os.makedirs('result')
    except FileExistsError:
        pass

        # save product list to json
    with open('result/product_list.json', 'w') as outfile:
        data_dict = {
            'list_product':list_product
        }

        json.dump(data_dict, outfile)

    print('write data success')
    # print(list_product)
    return list_product

# add treading function
thread_local = threading.local()

def get_sesion_thread():
    get_session_location()
    if not hasattr(thread_local, "session"):
        thread_local.session = session

    return thread_local.session


def get_detail(product):
    # getting session
    get_session_location()
    session = get_sesion_thread()
    # define global statement
    global data_product, index
    index += 1
    # print(index)

    # read data from productlist

    # define product url
    url = product['item link']

    # reguests
    # url = 'https://www.amazon.com/Wireless-Portable-Detection-Surveillance-Apartment/dp/B089DZY42R/ref=sr_1_31?dchild=1&keywords=camera&qid=1601261812&sr=8-31'
    print('getting detail {}. {} of {}'.format(index, url, len(data_product)))
    with session.get(url, headers=headers) as res:

        # checking if error
        f = open('./resp.html', 'w')
        f.write(res.text)
        f.close()
        # print('cheking complete')

        # res = session.get(url, headers=headers)
        soup = BeautifulSoup(res.text, 'html5lib')

        # price product
        # try:
        #     price = soup.find('span',
        #                       attrs={'id': 'price_inside_buybox', 'class': 'a-size-medium a-color-price'}).text.strip()

        # except:
        #     try:
        #         price = soup.find('span',
        #                           attrs={'class': 'a-size-base a-color-price offer-price a-text-normal'}).text.strip()
        #     except:
        #         try:
        #             price = soup.find('span', attrs={'id': 'newBuyBoxPrice',
        #                                              'class': 'a-size-base a-color-price header-price a-text-normal'}).text.strip()
        #         except:
        #             price = 'no price display'

        # getting seller name and seller profile
        seller = soup.find('a', attrs={'id': 'sellerProfileTriggerId'})
        try:
            seller_profile = seller.text.strip()
        except:
            try:
                seller_profile = soup.find('a', attrs={'class': 'a-link-normal', 'id': 'bylineInfo'}).text.strip()
            except:
                try:
                    seller_profile = soup.find('a', attrs={'class': 'a-spacing-top-small a-link-normal'})['href']
                    # print(seller_profile)
                except:
                    try:
                        seller_profile = soup.find('a', attrs={'class':'a-link-normal'}).text.strip()
                    except:
                        seller_profile = soup.find('span', recursive=True, string=False, attrs={'class':'author notFaded'}).find('a', attrs={'class':'a-link-normal'}).text.strip()
        try:
            seller_link = base_url + seller['href']
            # print(seller_link)
        except:
            try:
                seller_link = base_url + soup.find('a', attrs={'id': 'bylineInfo', 'class': 'a-link-normal'})['href']
                # print(seller_link)
            except:
                try:
                    seller_link = soup.find('a', attrs={'class': 'a-spacing-top-small a-link-normal'})['href']
                    # print(seller_link)
                except:
                    try:
                        seller_link = base_url + soup.find('a', attrs={'class':'a-link-normal'})['href']
                    except:
                        seller_link = 'no seller link found'

        # add data to product
        # product['price product'] = price
        product['seller profile'] = seller_profile
        product['seller link'] = seller_link

        return product

# configure threading
def download_all_sites(data_product):
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        executor_result = executor.map(get_detail, data_product)
        results = [result for result in executor_result]
        return results



def run():
    keywords = input('Enter search keywords: ')
    keywords = keywords.replace(' ', '+')

    # define global variable
    global data_product
    total = get_total_pages(keywords)
    for page in range(total):
        page +=1
        list_product = get_product_items(keywords=keywords, page=page)
        # print(list_product)
        data_product += list_product

        print('total item result saved: {}'.format(len(data_product)))
    # create directory to save result (json file)
    try:
        os.makedirs('result_json')
    except FileExistsError:
        pass

    with open('result_json/{}.json'.format(keywords), 'w') as outfile:
        json.dump(data_product, outfile)

    # read data result
    with open('./result_json/{}.json'.format(keywords), 'r') as json_file:
        data_product = json.load(json_file)

    # development mode
    # data_product = data_product[0:101]

    # getting all data
    results = download_all_sites(data_product)


    # raise  Exception(results)
    # print(results)

    # create csv and xlsx file with pandas
    df = pd.DataFrame(results)
    print('creating csv file')
    df.to_csv('{}.csv'.format(keywords), index=False)
    print('creating excel file')
    df.to_excel('{}.xlsx'.format(keywords), index=False)

if __name__=='__main__':
    run()
